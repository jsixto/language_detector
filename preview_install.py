__author__ = 'Juan.Sixto@deusto.es'

#**********************************************************************
#  Resource 'corpora/stopwords' not found.  Please use the NLTK
#  Downloader to obtain the resource:  >>> nltk.download()
#  Searched in:
#    - '/home/juan/nltk_data'
#    - '/usr/share/nltk_data'
#    - '/usr/local/share/nltk_data'
#    - '/usr/lib/nltk_data'
#    - '/usr/local/lib/nltk_data'
#**********************************************************************


import nltk

nltk.download()
