#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys
__author__ = 'Juan.Sixto@deusto.es'

'''
Based on http://blog.alejandronolla.com/ tutorial
'''

from nltk import wordpunct_tokenize
from nltk.corpus import stopwords
import sys

#----------------------------------------------------------------------
def _calculate_languages_ratios(text):
    """
    Calculate probability of given text to be written in several languages and
    return a dictionary that looks like {'french': 2, 'spanish': 4, 'english': 0}

    @param text: Text whose language want to be detected
    @type text: str

    @return: Dictionary with languages and unique stopwords seen in analyzed text
    @rtype: dict
    """

    languages_ratios = {}

    '''
    nltk.wordpunct_tokenize() splits all punctuations into separate tokens

    >>> wordpunct_tokenize("That's thirty minutes away. I'll be there in ten.")
    ['That', "'", 's', 'thirty', 'minutes', 'away', '.', 'I', "'", 'll', 'be', 'there', 'in', 'ten', '.']
    '''

    tokens = wordpunct_tokenize(text)
    words = [word.lower() for word in tokens]

    # Compute per language included in nltk number of unique stopwords appearing in analyzed text
    for language in stopwords.fileids():
        stopwords_set = set(stopwords.words(language))
        words_set = set(words)
        common_elements = words_set.intersection(stopwords_set)

        languages_ratios[language] = len(common_elements) # language "score"

    return languages_ratios


#----------------------------------------------------------------------
def detect_language(text):
    """
    Calculate probability of given text to be written in several languages and
    return the highest scored.

    It uses a stopwords based approach, counting how many unique stopwords
    are seen in analyzed text.

    @param text: Text whose language want to be detected
    @type text: str

    @return: Most scored language guessed
    @rtype: str
    """

    ratios = _calculate_languages_ratios(text)

    most_rated_language = max(ratios, key=ratios.get)

    return most_rated_language



if __name__=='__main__':

    english_text = '''
    There's a passage I got memorized. Ezekiel 25:17. "The path of the righteous man is beset on all sides\
    by the inequities of the selfish and the tyranny of evil men. Blessed is he who, in the name of charity\
    and good will, shepherds the weak through the valley of the darkness, for he is truly his brother's keeper\
    and the finder of lost children.
    '''
    spanish_text = '''
    La facilidad de deposito apenas era utilizada antes de la irrupcion de la crisis financiera, porque los bancos\
    preferian prestarse entre si a un dia, normalmente a un interes mayor del que ofrecia el BCE. Pero la falta de\
    confianza sobre lo que habia en el balance de otras entidades llevo a los bancos a restringir el uso del mercado\
    interbancario, incluso a los plazos mas cortos.
    '''

    german_text = '''
    Die Regierungen in Peking und Tokio machen sich gegenseitig für den Beinahezusammenstoß verantwortlich.\
    Japan bestellte den chinesischen Botschafter ein, Chinas Verteidigungsministerium warf hingegen der japanischen \
    Luftwaffe eine gefährliche Aktion vor, mit der sie die Sicherheit des chinesischen Flugzeugs bedroht habe.
    '''

    language = detect_language(english_text)
    print language
    language = detect_language(spanish_text)
    print language
    language = detect_language(german_text)
    print language